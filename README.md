# github-user-profiles-vue

## Project requirements
- Use Vuejs or React as the framework of choice
- Use the latest stable version of the framework
- Each user listed should be presented in a card component
(https://www.nngroup.com/articles/cards-component/)
- Use a state container (Vuex or Redux)
- Create some unit tests
- Create one e2e test using the tool of your choice
- Use a css library of your choice
- The app should display informational messages in case of error or some indication in
case of loading data.



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
