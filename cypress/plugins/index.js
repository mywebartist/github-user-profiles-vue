// cypress/plugins/index.js
const webpack = require('@cypress/webpack-dev-server')
const webpackOptions = {
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
        ],
    },
}

const options = {
    // send in the options from your webpack.config.js, so it works the same
    // as your app's code
    webpackOptions,
    watchOptions: {},
}

module.exports = (on) => {
    on('dev-server:start', webpack(options));


}