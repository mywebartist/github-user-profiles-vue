import { createRouter, createWebHistory } from 'vue-router'
import UsersListView from '../views/UsersListView.vue'

const routes = [
  {
    path: '/',
    name: 'usersListView',
    component: UsersListView
  },
  {
    path: '/user/:login',
    name: 'userDetailsView',
    component: () => import(/* webpackChunkName: "about" */ '../views/UserDetailsView')
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
