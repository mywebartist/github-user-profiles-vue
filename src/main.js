import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// ui
import 'mdb-vue-ui-kit/css/mdb.min.css';

// axios
import axios from 'axios'
import VueAxios from 'vue-axios'
// createApp(App).use(VueAxios, axios)

const app = createApp(App).use(store).use(router)
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)  // provide 'axios'


app.mount('#app')
