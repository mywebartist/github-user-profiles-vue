// https://docs.cypress.io/api/table-of-contents

describe('home page tests', () => {
  it('home title', () => {
    cy.visit('http://localhost:8080')
    cy.contains('[data-test="home-page-title"]', 'Github Users')
  })

  it('count user cards', () => {
    cy.get('[data-test="home-page-user-cards"]').children().should('have.length',10);
  })

  it('delete user', () => {
    cy.get('[data-test="user-thumb-card-delete-button"]').eq(1).click()
  })

})



