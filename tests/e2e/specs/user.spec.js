// https://docs.cypress.io/api/table-of-contents

describe('user page tests', () => {
  it('user title', () => {
    cy.visit('http://localhost:8080')
  })

  it('go to user page', () => {
    cy.get('[data-test="user-thumb-card-visit-button"]').eq(0).click()
  })

  it('user title', () => {
    cy.contains('[data-test="user-thumb-card-title"]', 'Github User')
  })

  it('user repo name', () => {
    cy.contains('[data-test="user-thumb-card-title"]', 'Repos by @mojombo')
  })
})



